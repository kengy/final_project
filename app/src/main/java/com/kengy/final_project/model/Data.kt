package com.kengy.final_project.model

class Data(
    var offset: Int,
    var limit: Int,
    var total: Int,
    var count: Int,
    var results: List<Results>
)