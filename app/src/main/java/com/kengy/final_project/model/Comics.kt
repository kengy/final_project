package com.kengy.final_project.model


class Comics(
    var available: String,
    var collectionURI: String,
    var items: List<Items>,
    var returned: Int

)