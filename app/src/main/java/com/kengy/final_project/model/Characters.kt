package com.kengy.final_project.model

class Characters(
    var available: Int,
    var collectionURI: String,
    var items: List<Items>,
    var returned: Int
)