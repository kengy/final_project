package com.kengy.final_project.model

class Thumbnail(
    var path: String,
    var extension: String
)