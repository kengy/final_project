package com.kengy.final_project.model

class StoriesResponse(
    var code: Int,
    var status: String,
    var copyright: String,
    var attributionText: String,
    var attributionHTML: String,
    var etag: String,
    var data: Data
)