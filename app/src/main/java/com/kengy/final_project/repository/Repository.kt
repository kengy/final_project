package com.kengy.final_project.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.kengy.final_project.api.ApiService
import com.kengy.final_project.commons.Utils
import com.kengy.final_project.database.DataBase
import com.kengy.final_project.database.Entities.CharactersEntity
import com.kengy.final_project.database.Entities.ComicsEntity
import com.kengy.final_project.database.Entities.SeriesEntity
import com.kengy.final_project.model.ResponseApi
import com.kengy.final_project.model.Results
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository(context: Context) {


    private val _apiPulbicKey = "133c10bc8be1dbd4a565eee86a528b06"
    var liveDataListComics = MutableLiveData<List<ComicsEntity>>()
    var liveDataListSeries = MutableLiveData<List<SeriesEntity>>()
    var liveDataListChar = MutableLiveData<List<CharactersEntity>>()
    private var _dao = DataBase.getInstance(context)

    fun getDataCharacterByName(name: String) {

        val ts = System.currentTimeMillis().toString()
        val retrofit = ApiService.getEndPointsCharacter()
        val call = retrofit.getCharacter(name, 100, ts, _apiPulbicKey, Utils.getHashMD5(ts))

        call.enqueue(object : Callback<ResponseApi> {
            override fun onFailure(call: Call<ResponseApi>, t: Throwable) {
                Log.i("deu ruim", "Deu ruim")
            }

            override fun onResponse(
                    call: Call<ResponseApi>,
                    response: Response<ResponseApi>
            ) {
                if (response.code() == 200) {
                    response.body()?.data?.results.let { it ->

                        val aux = mutableListOf<CharactersEntity>()
                        it?.forEach {
                            aux.add(
                                    CharactersEntity(
                                            it.id, it.name, Utils.makeImageUrl(
                                            "portrait_xlarge",
                                            it.thumbnail.extension,
                                            it.thumbnail.path
                                    ), it.description
                                    )
                            )
                        }
                        liveDataListChar.value = aux
                        doAsync {
                            _dao.Dao().addCharacter(aux)

                        }
                    }
                }
            }
        })
    }

    fun getDataComicsFromApí(idCharacter: Int) {

        val ts = System.currentTimeMillis().toString()
        val retrofit = ApiService.getEndPointsComics()
        val call = retrofit.getComics(idCharacter, 100, ts, _apiPulbicKey, Utils.getHashMD5(ts))

        call.enqueue(object : Callback<ResponseApi> {
            override fun onFailure(call: Call<ResponseApi>, t: Throwable) {
                Log.i("deu ruim", "Deu ruim")
            }

            override fun onResponse(
                    call: Call<ResponseApi>,
                    response: Response<ResponseApi>
            ) {


                if (response.code() == 200) {
                    response.body().let {
                        val aux = getListComicToBd(it?.data!!.results)

                        liveDataListComics.value = aux

                        doAsync {

                            _dao.Dao().addComics(aux)
                        }
                    }


                }
            }
        })
    }


    fun getDataSeriesFromApí(idCharacter: Int) {

        val ts = System.currentTimeMillis().toString()
        val retrofit = ApiService.getEndPointsSeries()
        val call = retrofit.getSeries(idCharacter, 100, ts, _apiPulbicKey, Utils.getHashMD5(ts))

        call.enqueue(object : Callback<ResponseApi> {
            override fun onFailure(call: Call<ResponseApi>, t: Throwable) {

                Log.i("deu ruim", "Deu ruim")
            }

            override fun onResponse(
                    call: Call<ResponseApi>,
                    response: Response<ResponseApi>
            ) {


                if (response.code() == 200) {
                    response.body().let {
                        val aux = getListSeriesToBd(it?.data!!.results)

                        liveDataListSeries.value = aux

                        doAsync {

                            _dao.Dao().addSeries(aux)
                        }
                    }


                }
            }
        })
    }


    private fun getListComicToBd(lstCom: List<Results>): MutableList<ComicsEntity> {
        val lstAux = mutableListOf<ComicsEntity>()

        lstCom.forEach { lstResultsComics ->
            lstResultsComics.characters?.items?.forEach {

                lstAux.add(
                        ComicsEntity(
                                lstResultsComics.id,
                                Integer.parseInt(getCharactersId(it.resourceURI)),
                                lstResultsComics.title,
                                Utils.makeImageUrl(
                                        "portrait_uncanny",
                                        lstResultsComics.thumbnail.extension,
                                        lstResultsComics.thumbnail.path
                                )

                        )
                )

            }

        }
        return lstAux

    }


    private fun getListSeriesToBd(lstSeries: List<Results>): MutableList<SeriesEntity> {
        val lstAux = mutableListOf<SeriesEntity>()

        lstSeries.forEach { lstResultsSeries ->
            lstResultsSeries.characters?.items?.forEach {

              lstAux.add(
                        SeriesEntity(lstResultsSeries.id,
                                Integer.parseInt(getCharactersId(it.resourceURI)),
                                lstResultsSeries.title,
                                Utils.makeImageUrl(
                                        "portrait_xlarge",
                                        lstResultsSeries.thumbnail.extension,
                                        lstResultsSeries.thumbnail.path
                                ),
                                if (lstResultsSeries.description == null) "" else lstResultsSeries.description,
                                lstResultsSeries.startYear,
                                lstResultsSeries.endYear

                        )
                )

            }

        }
        return lstAux

    }


    fun getDataComicsFromBd(idCharacter: Int) = _dao.Dao().loadComics(idCharacter)
    fun getDataSeriesFromBd(idCharacter: Int) = _dao.Dao().loadSeries(idCharacter)
    fun getListChar(nameChar: String?) = _dao.Dao().loadCharacter("%${nameChar?.trim()}%")


    private fun getCharactersId(url: String) = url.split("/")[6]

}