package com.kengy.final_project.ui

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.vectordrawable.graphics.drawable.ArgbEvaluator
import com.kengy.final_project.R
import com.kengy.final_project.commons.Utils
import kotlinx.android.synthetic.main.splash_sreen.*

class SplashActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_sreen)

        Handler().postDelayed({
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        }, 4000)


    }

    val timeoutSplashScreen: Long = 4000
    fun splashAnimateBackground(repeat: Int = 1, timeout: Long = timeoutSplashScreen) {

        val objectAnimator = ObjectAnimator.ofObject(
            lySplashScreen,
            "backgroundColor",
            ArgbEvaluator(),
            ContextCompat.getColor(
                this,
                R.color.colorPrimaryDark
            ),     ContextCompat.getColor(
                this,
                R.color.colorPrimaryDark
            ),
            ContextCompat.getColor(
                this,
                R.color.colorAccent

            )
        )

        objectAnimator.repeatCount = repeat
        objectAnimator.repeatMode = ValueAnimator.REVERSE
        objectAnimator.duration = timeout / repeat

        objectAnimator.start()
    }

}