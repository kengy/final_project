package com.kengy.final_project.ui.series

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.kengy.final_project.R
import com.kengy.final_project.commons.SharePreferences
import com.kengy.final_project.commons.Utils
import com.kengy.final_project.database.Entities.SeriesEntity
import kotlinx.android.synthetic.main.fragment_series.*

class SeriesFragment : Fragment() {

    private lateinit var seriesViewModel: SeriesViewModel
    private lateinit var _sharePref: SharePreferences


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        seriesViewModel =
            ViewModelProviders.of(this).get(SeriesViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_series, container, false)

        return root
    }



    override fun onResume() {
        super.onResume()
        _sharePref = SharePreferences(activity)

        if (Utils.isNetworkAvailable(context)) {
            showSeriesApi()
            seriesViewModel.getListSeriesFromApi(_sharePref.getCharId())


        } else {
            showSeriesBd()
        }

    }

    private fun showSeriesBd() {
        if (!seriesViewModel.getListSeriesFromBd().hasObservers()) {

            seriesViewModel.getListSeriesFromBd().observe(this, Observer {
                if (it.isNotEmpty() && it != null) {
                    rvSeries.layoutManager =LinearLayoutManager(context)
                    rvSeries.adapter = AdapterSeries(it)
                } else {
                    Utils.showErrosMessage("${_sharePref.getCharName()} has no Series avaliable", activity)
                }

                pbSeries.visibility = View.GONE

            })

        }
    }

    private fun showSeriesApi() {
        if (!seriesViewModel.getLisSeries().hasObservers()) {
            seriesViewModel.getLisSeries().observe(this, Observer {

                if (it.isNotEmpty() && it != null) {
                    val listSeries = mutableListOf<SeriesEntity>()
                    it.forEach {
                        if (it.idCharacter == _sharePref.getCharId())
                            listSeries.add(it)
                    }
                    rvSeries.layoutManager =
                            LinearLayoutManager(context)
                    rvSeries.adapter = AdapterSeries(listSeries)
                } else {
                    Utils.showErrosMessage("${_sharePref.getCharName()} has no Series avaliable", activity)

                }
                pbSeries.visibility = View.GONE


            })
        }

    }
}