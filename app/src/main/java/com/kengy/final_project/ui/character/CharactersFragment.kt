package com.kengy.final_project.ui.character

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kengy.final_project.R
import com.kengy.final_project.commons.SharePreferences
import com.kengy.final_project.commons.Utils
import com.kengy.final_project.database.Entities.CharactersEntity
import com.kengy.final_project.ui.BottomNavActivity
import kotlinx.android.synthetic.main.fragment_characters.*

class CharactersFragment : Fragment() {

    private lateinit var viewModelChar: CharactersViewModel
    var prefCharacter: SharePreferences? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_characters, container, false)

    }


    override fun onResume() {
        super.onResume()

        viewModelChar = ViewModelProviders.of(this).get(CharactersViewModel::class.java)

         val _pref = SharePreferences(activity)
        btnSeachChar.setOnClickListener {

            _pref.setCharName(edtCharacter.text.toString().trim())
            btnSeachChar.visibility = View.GONE
            pbCharacter.visibility = View.VISIBLE
            Utils.hidderKeyBoard(activity, btnSeachChar)

            if (!Utils.isNetworkAvailable(context)) {

                showCharactersDb(edtCharacter.text.toString().trim())
            } else {

                showCharactersApi()

                viewModelChar.getCharacters(edtCharacter.text.toString())
            }

        }

        edtCharacter.setOnClickListener {
            btnSeachChar.visibility = View.VISIBLE
            Utils.showKeyBoard(activity, edtCharacter)

        }

    }

    fun clearAdapter(recyclerView: RecyclerView){
        if (recyclerView.adapter != null)
        recyclerView.adapter = null
    }
    private fun showCharactersApi() {

        clearAdapter(rvCharacters)

        if (!viewModelChar.getListCharFromApi().hasObservers())
        viewModelChar.getListCharFromApi().observe(this, Observer {

            if (it != null && it.isNotEmpty()) {

                rvCharacters.layoutManager = LinearLayoutManager(context)
                rvCharacters.adapter = AdapterCharacters(
                        it,
                        { results: CharactersEntity -> Utils.openImage(results, activity) },
                        { results: CharactersEntity -> callBottomNavFrag(results) }
                )
                pbCharacter.visibility = View.GONE
            } else {
                Utils.showErrosMessage("Character was not Found", activity)
                pbCharacter.visibility = View.GONE
            }
        })
    }

    private fun showCharactersDb(nameChar: String) {

        clearAdapter(rvCharacters)

         if (!viewModelChar.getListCharFromDb().hasObservers()){
        viewModelChar.getListCharFromDb().observe(this, Observer {

            if (it.isEmpty() || it == null) {

                Utils.showErrosMessage("Character was not Found", activity)
                pbCharacter.visibility = View.GONE
                rvCharacters.adapter = null

            } else {

                rvCharacters.layoutManager = LinearLayoutManager(context)
                rvCharacters.adapter = AdapterCharacters(
                        it,
                        { results: CharactersEntity -> Utils.openImage(results, activity) },
                        { results: CharactersEntity -> callBottomNavFrag(results) }
                )
                pbCharacter.visibility = View.GONE
            }
        })}
    }

    private fun callBottomNavFrag(character: CharactersEntity) {
        prefCharacter = SharePreferences(context)
        prefCharacter!!.setCharId(character.id)
        startActivity(Intent(context, BottomNavActivity::class.java))
    }


}