package com.kengy.final_project.ui.character

import android.app.Application
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import com.kengy.final_project.commons.SharePreferences
import com.kengy.final_project.repository.Repository

class CharactersViewModel(application: Application):AndroidViewModel(application) {

    private val _repository = Repository(application)
    private val _sharePref = SharePreferences(application)

    fun getCharacters (name:String) = _repository.getDataCharacterByName(name)
    fun getListCharFromDb() = _repository.getListChar(_sharePref.getCharName())
    fun getListCharFromApi() = _repository.liveDataListChar




}