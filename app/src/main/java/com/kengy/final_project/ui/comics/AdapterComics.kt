package com.kengy.final_project.ui.comics

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.kengy.final_project.R
import com.kengy.final_project.model.Results
import com.kengy.final_project.commons.Utils
import com.kengy.final_project.database.Entities.ComicsEntity
import kotlinx.android.synthetic.main.list_comics_item.view.*

class AdapterComics(val dataSetComics: List<ComicsEntity>) :
    RecyclerView.Adapter<AdapterComics.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.list_comics_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = dataSetComics.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(dataSetComics[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindData(comics: ComicsEntity) {

            itemView.txtComicTitle.text = comics.title
            itemView.imgComic.load(comics.urlImage)

        }

    }
}