package com.kengy.final_project.ui.stories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.kengy.final_project.R

class StoriesFragment : Fragment() {

    private lateinit var storiesViewModel: StoriesViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        storiesViewModel =
            ViewModelProviders.of(this).get(StoriesViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_stories, container, false)
        val textView: TextView = root.findViewById(R.id.text_home)
        storiesViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}