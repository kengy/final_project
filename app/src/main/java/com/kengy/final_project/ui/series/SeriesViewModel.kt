package com.kengy.final_project.ui.series

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.kengy.final_project.commons.SharePreferences
import com.kengy.final_project.repository.Repository

class SeriesViewModel(application: Application) : AndroidViewModel(application) {

    private val _repository = Repository(application)
    private val _sharePref = SharePreferences(application)
    private val _listSeries = _repository.liveDataListSeries


    fun getLisSeries() = _listSeries
    fun getListSeriesFromApi(idChar : Int)=_repository.getDataSeriesFromApí(idChar)
    fun getListSeriesFromBd () = _repository.getDataSeriesFromBd(_sharePref.getCharId())
}