package com.kengy.final_project.ui.comics

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.kengy.final_project.commons.SharePreferences
import com.kengy.final_project.repository.Repository

class ComicsViewModel(application: Application) : AndroidViewModel(application) {

    private val _repository = Repository(application)
    private val _listComics =  _repository.liveDataListComics
    private val _sharePref = SharePreferences(application)


    fun getLisComics() = _listComics
    fun getListComicsFromApi(idChar : Int)=_repository.getDataComicsFromApí(idChar)
    fun getListComicsFromBd () = _repository.getDataComicsFromBd(_sharePref.getCharId())
}