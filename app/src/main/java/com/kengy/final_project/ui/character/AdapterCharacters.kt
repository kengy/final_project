package com.kengy.final_project.ui.character

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.kengy.final_project.R
import com.kengy.final_project.model.Results
import com.kengy.final_project.commons.Utils
import com.kengy.final_project.database.Entities.CharactersEntity
import kotlinx.android.synthetic.main.list_character_item.view.*

class AdapterCharacters(
    private val dataSetChar: List<CharactersEntity>,
    private val onClick: (CharactersEntity) -> Unit,
    private val onClickDescription: (CharactersEntity) -> Unit
) :
    RecyclerView.Adapter<AdapterCharacters.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater =
            LayoutInflater.from(parent.context).inflate(R.layout.list_character_item, parent, false)

        return ViewHolder(layoutInflater)
    }

    override fun getItemCount() = dataSetChar.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindData(dataSetChar[position], onClick, onClickDescription)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindData(
            character: CharactersEntity,
            onClick: (CharactersEntity) -> Unit,
            onClickDescription: (CharactersEntity) -> Unit
        ) {

            itemView.imgCharacter.setOnClickListener { onClick(character) }
            itemView.constLyListItem.setOnClickListener{onClickDescription(character)}
            itemView.txtNomeCharacter.text = character.name
            itemView.imgCharacter.load(character.urlImage)
        }

    }
}