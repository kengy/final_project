package com.kengy.final_project.ui.series

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.kengy.final_project.R
import com.kengy.final_project.database.Entities.SeriesEntity
import kotlinx.android.synthetic.main.list_series_item.view.*


class AdapterSeries(val dataSetSeries: List<SeriesEntity>) :
        RecyclerView.Adapter<AdapterSeries.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
                LayoutInflater.from(parent.context).inflate(R.layout.list_series_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = dataSetSeries.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(dataSetSeries[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindData(serie: SeriesEntity) {

            itemView.txtSerieTitle.text = serie.title
            itemView.txtEndYear.text = serie.endYear.toString()
            itemView.txtStartYear.text = serie.startYear.toString()
            itemView.imgSerie.load(serie.urlImage)

        }

    }
}