package com.kengy.final_project.ui.comics

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.kengy.final_project.R
import com.kengy.final_project.commons.SharePreferences
import com.kengy.final_project.commons.Utils
import com.kengy.final_project.database.Entities.ComicsEntity
import kotlinx.android.synthetic.main.fragment_comics.*
import okhttp3.internal.Util

class ComicsFragment : Fragment() {

    private lateinit var comicsViewModel: ComicsViewModel
    private lateinit var _sharePref: SharePreferences


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        comicsViewModel =
                ViewModelProviders.of(this).get(ComicsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_comics, container, false)

        return root
    }

    override fun onResume() {
        super.onResume()
        _sharePref = SharePreferences(activity)

        if (Utils.isNetworkAvailable(context)) {
            comicsViewModel.getListComicsFromApi(_sharePref.getCharId())
            showComicsApi()

        } else {
            showComicsBd()
        }

    }

    private fun showComicsBd() {
        if (!comicsViewModel.getListComicsFromBd().hasObservers()) {

            comicsViewModel.getListComicsFromBd().observe(this, Observer {
                if (it.isNotEmpty() && it != null) {
                    rvComics.layoutManager =
                            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    rvComics.adapter = AdapterComics(it)
                } else {
                    Utils.showErrosMessage("${_sharePref.getCharName()} has no Comics avaliable", activity)
                }

                pbComics.visibility = View.GONE

            })

        }
    }

    private fun showComicsApi() {
        if (!comicsViewModel.getLisComics().hasObservers()) {
            comicsViewModel.getLisComics().observe(this, Observer {

                if (it.isNotEmpty() && it != null) {
                    val listComics = mutableListOf<ComicsEntity>()
                    it.forEach {
                        if (it.idCharacter == _sharePref.getCharId())
                            listComics.add(it)
                    }
                    rvComics.layoutManager =
                            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    rvComics.adapter = AdapterComics(listComics)
                } else {
                    Utils.showErrosMessage("${_sharePref.getCharName()} has no Comics avaliable", activity)

                }
                pbComics.visibility = View.GONE


            })
        }

    }
}