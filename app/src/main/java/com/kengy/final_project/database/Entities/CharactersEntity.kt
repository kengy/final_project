package com.kengy.final_project.database.Entities

import androidx.room.Embedded
import androidx.room.Entity

@Entity(tableName = "characters",primaryKeys = ["id" , "name"])
class CharactersEntity (
    val id : Int,
    val name : String,
    val urlImage : String,
    val description : String
)