package com.kengy.final_project.database.Entities

import androidx.room.Entity

@Entity(tableName = "series", primaryKeys = ["idSerie", "idCharacter"])
class SeriesEntity(
        val idSerie: Int,
        val idCharacter: Int,
        val title: String,
        val urlImage: String,
        val description: String,
        val startYear: Int,
        val endYear: Int
)
