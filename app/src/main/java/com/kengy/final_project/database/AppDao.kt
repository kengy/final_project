package com.kengy.final_project.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kengy.final_project.database.Entities.CharactersEntity
import com.kengy.final_project.database.Entities.ComicsEntity
import com.kengy.final_project.database.Entities.SeriesEntity

@Dao
interface AppDao {

    @Query(value = "select * from characters where name like :parans")
    fun loadCharacter(parans: String): LiveData<List<CharactersEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCharacter(char: MutableList<CharactersEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addComics(listComicToBd: MutableList<ComicsEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addSeries(listSeriesToBd: MutableList<SeriesEntity>)

    @Query(value = "select * from series where idCharacter = :idCharacter")
    fun loadSeries(idCharacter: Int): LiveData<List<SeriesEntity>>

    @Query(value = "select * from comics where idCharacter = :idCharacter")
    fun loadComics(idCharacter: Int): LiveData<List<ComicsEntity>>


}