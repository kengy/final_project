package com.kengy.final_project.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.kengy.final_project.database.Entities.CharactersEntity
import com.kengy.final_project.database.Entities.ComicsEntity
import com.kengy.final_project.database.Entities.SeriesEntity
import com.kengy.final_project.model.Data

@Database(entities = [CharactersEntity::class, ComicsEntity::class, SeriesEntity::class], version = 2, exportSchema = false)
abstract class DataBase : RoomDatabase() {

    abstract fun Dao(): AppDao

    companion object {
        var INSTANCE: DataBase? = null
        fun getInstance(context: Context): DataBase {

            return if (INSTANCE == null) {

                INSTANCE = Room.databaseBuilder(
                    context,
                    DataBase::class.java,
                    "databaseMarvel.db"
                ).build()

                INSTANCE as DataBase
            } else
                INSTANCE as DataBase
        }
    }
}