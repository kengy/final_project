package com.kengy.final_project.database.Entities

import androidx.room.Entity

@Entity(tableName = "comics",primaryKeys = ["idComics","idCharacter"])
class ComicsEntity(
    val idComics: Int,
    val idCharacter: Int,
    val title: String,
    val urlImage:String
)




