package com.kengy.final_project.api

import com.kengy.final_project.model.ResponseApi
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SeriesEndPoints {

    @GET("/v1/public/characters/{characterId}/series")
    fun getSeries(
            @Path("characterId") characterId: Int,
            @Query("limit") limit: Int,
            @Query("ts") ts: String,
            @Query("apikey") apikey: String,
            @Query("hash") hash: String
    ): Call<ResponseApi>
}
