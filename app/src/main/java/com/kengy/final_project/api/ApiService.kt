package com.kengy.final_project.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiService {

    private const val url = "https://gateway.marvel.com"

    private fun getRetrofit(): Retrofit{

        return  Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getEndPointsCharacter(): CharEndpoints {
        return  getRetrofit().create(CharEndpoints::class.java)
    }

    fun getEndPointsComics(): ComicsEndoints {
        return  getRetrofit().create(ComicsEndoints::class.java)
    }

    fun getEndPointsSeries(): SeriesEndPoints {
        return  getRetrofit().create(SeriesEndPoints::class.java)
    }
}