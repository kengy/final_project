package com.kengy.final_project.api

import com.kengy.final_project.model.ResponseApi
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface CharEndpoints {

    @GET("/v1/public/characters")
    fun getCharacter (@Query ("nameStartsWith") nameStartsWith : String,
                      @Query ("limit") limit : Int,
                      @Query ("ts") ts : String ,
                      @Query ("apikey") apikey: String,
                      @Query("hash") hash : String) : Call<ResponseApi>
}