package com.kengy.final_project.commons

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.kengy.final_project.R
import kotlinx.android.synthetic.main.fragment_dlg_not_found.view.*

class DialogNotFound(var message: String) : DialogFragment() {


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = activity?.layoutInflater!!.inflate(R.layout.fragment_dlg_not_found, null)
        view.txtMessageNotFound.text = message

        val dialog = AlertDialog.Builder(activity)


        dialog.setView(view)
        return dialog.create()
    }
}