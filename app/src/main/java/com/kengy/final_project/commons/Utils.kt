package com.kengy.final_project.commons

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.kengy.final_project.database.Entities.CharactersEntity
import kotlinx.android.synthetic.main.fragment_characters.*
import java.math.BigInteger
import java.security.MessageDigest


object Utils   {

    val apiPulbicKey = "133c10bc8be1dbd4a565eee86a528b06"
    val apiPrivateKey = "d8ee0cbca93b91db7657f7855ef37a6c1440912f"
    lateinit var dlgErro : DialogNotFound
    private lateinit var _genericImagesDialog: GenericImagesDialog


    fun makeGif(imgView: ImageView, context: Context) {

        Glide.with(context).load("https://media.giphy.com/media/RS73cthFKKaw8/giphy.gif")
            .into(imgView)
    }

    fun getHashMD5(ts: String) = "$ts$apiPrivateKey$apiPulbicKey".md5()


    fun String.md5(): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
    }

    fun makeImageUrl(dimension: String, extension: String, path: String) =
        "$path/${dimension}.$extension"


     fun isNetworkAvailable(context: Context?): Boolean {
        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE)

        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    fun showErrosMessage(message:String, activity:FragmentActivity? ){
        dlgErro = DialogNotFound(message)
        dlgErro.show(activity!!.supportFragmentManager, "Teste")
    }

    fun openImage(result: CharactersEntity, activity: FragmentActivity?) {

        _genericImagesDialog = GenericImagesDialog(result)
        _genericImagesDialog.show(activity!!.supportFragmentManager, "Teste")
    }

    fun hidderKeyBoard(context: FragmentActivity?, view: View) {

        val inputMethodManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)

    }

    fun showKeyBoard(context: FragmentActivity?, view: View) {

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, 0)
        view.requestFocus()

    }


}