package com.kengy.final_project.commons

import android.content.Context

class SharePreferences(context: Context?) {

    companion object {
        val PREF_CHARACTER = "com.kengy.marvelsystem.prefs"
        val pref_id_character = "pref_id_character"
        val pref_name_character = "pref_name_character"

    }

    val preference = context?.getSharedPreferences(PREF_CHARACTER, Context.MODE_PRIVATE)

    fun getCharId() = preference!!.getInt(pref_id_character, 0)


    fun getCharName() = preference!!.getString(pref_name_character, "")


    fun setCharId(charId: Int) {
        val editor = preference?.edit()
        editor?.putInt(pref_id_character, charId)
        editor?.apply()
    }

    fun setCharName(charName: String) {
        val editor = preference?.edit()
        editor?.putString(pref_name_character, charName)
        editor?.apply()
    }
}