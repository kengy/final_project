package com.kengy.final_project.commons

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import coil.api.load
import com.kengy.final_project.R
import com.kengy.final_project.database.Entities.CharactersEntity
import com.kengy.final_project.model.Results
import kotlinx.android.synthetic.main.fragment_dialog_img_char.view.*

class GenericImagesDialog(val result: CharactersEntity): DialogFragment() {



    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = activity?.layoutInflater!!.inflate(R.layout.fragment_dialog_img_char,null)



        view.imgDialogCharacter.load(result.urlImage)
        view.txtDescription.text = result.description

        val dialog = AlertDialog.Builder(activity)

        dialog
            //.setTitle("Description").setCustomTitle()
//            .setPositiveButton("Passo", {teste, teste1 ->
//
//        }).setCancelable(false)
            .setView(view)
        return dialog.create()

    }
}